﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeeNumber.Repository
{
    public interface IRepository<T> where T : class
    {
        Task<IQueryable<T>> GetAll();

        Task<T> FindById(Guid id);

        Task Insert(T entity);

        Task InsertRange(IEnumerable<T> entities);
        Task UpdateRange(IEnumerable<T> entities);

        Task Update(T entity);

        Task Delete(T entity);

        T Find(Func<T, bool> predicate);
        Task<IQueryable<T>> FindAll(Func<T, bool> predicate);
        Task DeleteRange(IEnumerable<T> entities);
        Task SoftDelete(T entity);
    }
}
