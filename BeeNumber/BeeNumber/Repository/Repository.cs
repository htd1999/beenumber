﻿using BeeNumber.Context;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BeeNumber.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected internal readonly DataContext _context;
        protected internal readonly DbSet<T> _dbSet;

        public Repository(DataContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public virtual async Task Delete(T entity)
            => await Task.Run(() => _dbSet.Remove(entity));

        public virtual async Task DeleteRange(IEnumerable<T> entities)
            => await Task.Run(() => _dbSet.RemoveRange(entities));

        public virtual T Find(Func<T, bool> predicate) => _dbSet.FirstOrDefault(predicate);

        public virtual async Task<IQueryable<T>> FindAll(Func<T, bool> predicate)
            => await Task.Run<IQueryable<T>>(() => _dbSet.Where<T>(predicate).AsQueryable());

        public virtual async Task<T> FindById(Guid id) => await Task.Run<T>(async () =>  await _dbSet.FindAsync(id));

        public virtual async Task<IQueryable<T>> GetAll() => await Task.Run<IQueryable<T>>(() => _dbSet.AsQueryable<T>());

        public virtual async Task Insert(T entity) => await _dbSet.AddAsync(entity);

        public virtual async Task InsertRange(IEnumerable<T> entities) => await _dbSet.AddRangeAsync(entities);
        

        public virtual async Task SoftDelete(T entity) =>
        
            await Task.Run(() => {
                PropertyInfo entityInstance = entity.GetType().GetProperty("IsActive");
                if (entityInstance != null)
                {
                    entityInstance.SetValue(entity, false);
                }

            });
            
        

        public virtual async Task Update(T entity) => await Task.Run(() => _dbSet.Update(entity));

        public virtual async Task UpdateRange(IEnumerable<T> entities) => await Task.Run(() => _dbSet.UpdateRange(entities));

    }
}
