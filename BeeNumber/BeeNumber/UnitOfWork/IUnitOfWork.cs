﻿using BeeNumber.Repository;

namespace BeeNumber.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T : class;
        int Commit();
    }
}