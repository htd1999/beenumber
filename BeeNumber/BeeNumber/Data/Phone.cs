﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeeNumber.Data
{
    public class Phone
    {
        public Phone()
        {

        }
        public Phone(string _Number)
        {
            this.Number = _Number;
        }

        public string Number { get; set; }
    }
}
