﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeeNumber.Data
{
    public class NetworkProvider
    {
        public NetworkProvider()
        {
            Prefixs = new HashSet<PrefixNP>();
        }
        public NetworkProvider(Guid id, string name)
        {
            ID = id;
            Name = name;
        }
        public Guid ID { get; set; }
        public string Name { get; set; }

        public virtual IEnumerable<PrefixNP> Prefixs { get; set; }
    }
}
