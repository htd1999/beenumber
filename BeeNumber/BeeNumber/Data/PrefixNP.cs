﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeeNumber.Data
{
    public class PrefixNP
    {
        public PrefixNP(string prefix, Guid networkProviderID)
        {
            Prefix = prefix;
            NetworkProviderID = networkProviderID;
        }
        public string Prefix { get; set; }

        public Guid NetworkProviderID { get; set; }
        public virtual NetworkProvider NetworkProvider { get; set; }
    }
}
