﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BeeNumber
{
    public class PrepareConfig
    {
        private static string file = "beeconfiguation.json";

        public static JObject Read()
        {
            try
            {
                return JObject.Parse(File.ReadAllText(file));
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found!!!");
                return null;
            }
        }

        public static void Check(JObject test)
        {
            try
            {
                test.GetValue("maxlength").ToList();
                test.GetValue("minlength").ToList();
                test.GetValue("taboo2LastNumber").ToList();
                test.GetValue("sumDivide5first5last").ToList();
                test.GetValue("lastNicePair").ToList();
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException("File config is missing some parameter!!!");
            }
        }
    }
}
