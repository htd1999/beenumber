﻿using BeeNumber.Context;
using BeeNumber.Data;
using BeeNumber.UnitOfWork;
using Bogus;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace BeeNumber
{
    public class Program
    {
        private static JObject pc = PrepareConfig.Read();
        private static IUnitOfWork _unitOfWork;

        private static void prepareService()
        {
            //Service DI test
            ServiceCollection services = new ServiceCollection();
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlServer("Server=LAPTOP-L0VL03GE;Database=BeeNumber;user id=sa;password=123456;Trusted_Connection=false;");
                options.UseLazyLoadingProxies();
            });
            services.AddScoped<IUnitOfWork, UnitOfWork.UnitOfWork>();
            var provider = services.BuildServiceProvider();
            _unitOfWork = provider.GetService<IUnitOfWork>();
        }

        static void Main(string[] args)
        {
            //Check Config
            try
            {
                PrepareConfig.Check(pc);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            //Prepare Service
            prepareService();

            #region Process
            //Get All data
            var phoneList = _unitOfWork.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();

            //Check 10 limit length and must be number
            phoneList = checkNumerAndLong(phoneList);

            //Check in network provider
            try
            {
                phoneList = checkNetworkProvider(phoneList);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }


            //check taboo 2 last number
            phoneList = Check2LastNumberTaboo(phoneList);

            //check Conditon Sum First5/Last5 
            phoneList = phoneList.Where(_ => CheckConditonFirst5Last5(_.Number));

            //Check Last nice pair of numbers
            phoneList = Check2LastNicePair(phoneList);
            #endregion Process

            //Show result
            if (phoneList.Any())
            {
                Console.WriteLine("List Bee number: \n");
                Console.WriteLine(phoneList.Select(_ => _.Number).Aggregate((i, j) => i + "\n" + j));
            }
            else
            {
                Console.WriteLine("There is no Bee number in DB");
            }

        }

        public static bool CheckFormat(string temp, string format)
        {
            try
            {
                var r = new Regex(format);
                return r.IsMatch(temp);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CheckConditonFirst5Last5(string temp)
        {
            if (temp == null || temp.Trim().Equals("")) return false;
            var first5 = temp.ToList().Take(5).Sum(_ => int.Parse(_.ToString()));
            var last5 = temp.ToList().Skip(Math.Max(0, temp.Count() - 5)).Sum(_ => int.Parse(_.ToString()));
            var result = first5 + "/" + last5;
            var listCondition = pc["sumDivide5first5last"].ToList().Select(_ => _.ToString());
            listCondition = listCondition.Where(_ => _.Equals(result));
            return listCondition.Count() > 0;
        }

        public static IEnumerable<Phone> checkNumerAndLong(IEnumerable<Phone> listCheck)
        {
            var formatRegex = "^[0-9]{" + pc["minlength"] + "," + pc["maxlength"] + "}$";
            return listCheck.Where(_ => CheckFormat(_.Number, formatRegex));
        }

        public static IEnumerable<Phone> checkNetworkProvider(IEnumerable<Phone> listCheck)
        {
            var prefixNP = _unitOfWork.GetRepository<PrefixNP>().GetAll().GetAwaiter().GetResult().ToList();
            try
            {
                string prefixFormat = "^(" + prefixNP.Select(_ => _.Prefix).Aggregate((i, j) => i + "|" + j) + ")";
                return listCheck.Where(_ => CheckFormat(_.Number, prefixFormat));
            }
            catch(ArgumentNullException)
            {
                throw new Exception("Missing Network Provider Prefix");
            }
        }

        public static IEnumerable<Phone> Check2LastNumberTaboo(IEnumerable<Phone> listCheck)
        {
            string postFix = "(" + pc["taboo2LastNumber"].ToList().Aggregate((i, j) => i + "|" + j) + ")$";
            listCheck = listCheck.Where(_ => !CheckFormat(_.Number, postFix));
            listCheck = listCheck.Where(_ => _.Number != null && !_.Number.Trim().Equals("")).ToList();
            return listCheck;
        }
        
        public static IEnumerable<Phone> Check2LastNicePair(IEnumerable<Phone> listCheck)
        {
            string lastNicePairRegex = "(" + pc["lastNicePair"].ToList().Aggregate((i, j) => i + "|" + j) + ")$";
            return listCheck.Where(_ => CheckFormat(_.Number, lastNicePairRegex));
        }

        
    }
}
