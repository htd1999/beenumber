﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BeeNumber.Migrations
{
    public partial class init1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NetworkProvider",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NetworkProvider", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Phone",
                columns: table => new
                {
                    Number = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Phone", x => x.Number);
                });

            migrationBuilder.CreateTable(
                name: "PrefixNP",
                columns: table => new
                {
                    Prefix = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: false),
                    NetworkProviderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrefixNP", x => x.Prefix);
                    table.ForeignKey(
                        name: "FK_PrefixNP_NetworkProvider_NetworkProviderID",
                        column: x => x.NetworkProviderID,
                        principalTable: "NetworkProvider",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "NetworkProvider",
                columns: new[] { "ID", "Name" },
                values: new object[,]
                {
                    { new Guid("2e800cc4-668d-43ad-9d14-cedda6933535"), "Viettel" },
                    { new Guid("4f1d0134-3097-4e27-b657-1e38c06b1aba"), "Mobi" },
                    { new Guid("9b92296a-353d-4027-be09-7b259db02d56"), "Vinaphone" }
                });

            migrationBuilder.InsertData(
                table: "Phone",
                column: "Number",
                values: new object[]
                {
                    "09492737",
                    "09492837",
                    "094923537",
                    "094922637",
                    "094922537",
                    "094923437",
                    "0863476712",
                    "09159924",
                    "0863476710",
                    "0863476700",
                    "0863476706",
                    "0863476797",
                    "0863476798",
                    "039923411",
                    "09159824",
                    "09159626",
                    "0863476742",
                    "01234567891",
                    "012345",
                    "01234",
                    "0123",
                    "0947479634",
                    "0947476934",
                    "09159726",
                    "0893456719",
                    "0947465737",
                    "0947465752",
                    "0863476719",
                    "08619819",
                    "08619919",
                    "08619937",
                    "0893475619",
                    "08676712"
                });

            migrationBuilder.InsertData(
                table: "PrefixNP",
                columns: new[] { "Prefix", "NetworkProviderID" },
                values: new object[,]
                {
                    { "086", new Guid("2e800cc4-668d-43ad-9d14-cedda6933535") },
                    { "096", new Guid("2e800cc4-668d-43ad-9d14-cedda6933535") },
                    { "097", new Guid("2e800cc4-668d-43ad-9d14-cedda6933535") },
                    { "091", new Guid("2e800cc4-668d-43ad-9d14-cedda6933535") },
                    { "094", new Guid("2e800cc4-668d-43ad-9d14-cedda6933535") },
                    { "089", new Guid("4f1d0134-3097-4e27-b657-1e38c06b1aba") },
                    { "090", new Guid("4f1d0134-3097-4e27-b657-1e38c06b1aba") },
                    { "093", new Guid("4f1d0134-3097-4e27-b657-1e38c06b1aba") },
                    { "088", new Guid("9b92296a-353d-4027-be09-7b259db02d56") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_PrefixNP_NetworkProviderID",
                table: "PrefixNP",
                column: "NetworkProviderID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PrefixNP");

            migrationBuilder.DropTable(
                name: "Phone");

            migrationBuilder.DropTable(
                name: "NetworkProvider");
        }
    }
}
