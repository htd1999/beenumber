﻿using BeeNumber.Data;
using Bogus;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeeNumber.Context
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        
        public DbSet<Phone> Phone { get; set; }
        public DbSet<NetworkProvider> NetworkProvider { get; set; }
        public DbSet<PrefixNP> PrefixNP { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseLazyLoadingProxies();
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=LAPTOP-L0VL03GE;Database=BeeNumber;user id=sa;password=123456;Trusted_Connection=false;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Phone>(entity =>
            {
                entity.ToTable("Phone");
                entity.HasKey(_ => _.Number);
            });
            

            modelBuilder.Entity<PrefixNP>(entity =>
            {
                entity.ToTable("PrefixNP");
                entity.HasKey(_ => _.Prefix);
                entity.Property(_ => _.Prefix).HasMaxLength(3);
                entity.HasOne(p => p.NetworkProvider)
                    .WithMany(n => n.Prefixs)
                    .HasForeignKey(p => p.NetworkProviderID);
            });

            modelBuilder.Entity<NetworkProvider>(entity =>
            {
                entity.ToTable("NetworkProvider");
                entity.HasKey(_ => _.ID);
                entity.Property(_ => _.Name)
                    .IsRequired()
                    .HasMaxLength(10);
                entity.HasMany(n => n.Prefixs)
                    .WithOne(p => p.NetworkProvider)
                    .HasForeignKey(p => p.NetworkProviderID);
            });

            List<Phone> phoneList = new List<Phone>() { 
                new Phone { Number = "01234567891"},
                new Phone { Number = "012345"},
                new Phone { Number = "01234"},
                new Phone { Number = "0123"},
                new Phone { Number = "0947479634"},
                new Phone { Number = "0947476934"},
                new Phone { Number = "0893456719"},
                new Phone { Number = "0893475619"},
                new Phone { Number = "0947465737"},
                new Phone { Number = "0947465752"},
                new Phone { Number = "0863476719"},
                new Phone { Number = "08619819"},
                new Phone { Number = "08619919"},
                new Phone { Number = "08619937"},
                new Phone { Number = "09159626"},
                new Phone { Number = "09159726"},
                new Phone { Number = "09159824"},
                new Phone { Number = "09159924"},
                new Phone { Number = "09492737"},
                new Phone { Number = "09492837"},
                new Phone { Number = "094923537"},
                new Phone { Number = "094922637"},
                new Phone { Number = "094922537"},
                new Phone { Number = "094923437"},
                new Phone { Number = "039923411"},
                new Phone { Number = "0863476712"},
                new Phone { Number = "0863476710"},
                new Phone { Number = "0863476700"},
                new Phone { Number = "0863476706"},
                new Phone { Number = "0863476797"},
                new Phone { Number = "0863476798"},
                new Phone { Number = "0863476742"},
                new Phone { Number = "08676712"}
            };
            modelBuilder.Entity<Phone>().HasData(phoneList);

            List<NetworkProvider> npList = new List<NetworkProvider>() {
                new NetworkProvider { ID = Guid.NewGuid(), Name = "Viettel"},
                new NetworkProvider { ID = Guid.NewGuid(), Name = "Mobi"},
                new NetworkProvider { ID = Guid.NewGuid(), Name = "Vinaphone"}
            };
            modelBuilder.Entity<NetworkProvider>().HasData(npList);

            List<PrefixNP> prList = new List<PrefixNP>() {
                new PrefixNP ("086",  npList.FirstOrDefault(_ => _.Name.Equals("Viettel")).ID),
                new PrefixNP ("096",  npList.FirstOrDefault(_ => _.Name.Equals("Viettel")).ID),
                new PrefixNP ("097",  npList.FirstOrDefault(_ => _.Name.Equals("Viettel")).ID),
                new PrefixNP ("089",  npList.FirstOrDefault(_ => _.Name.Equals("Mobi")).ID),
                new PrefixNP ("090",  npList.FirstOrDefault(_ => _.Name.Equals("Mobi")).ID),
                new PrefixNP ("093",  npList.FirstOrDefault(_ => _.Name.Equals("Mobi")).ID),
                new PrefixNP ("088",  npList.FirstOrDefault(_ => _.Name.Equals("Vinaphone")).ID),
                new PrefixNP ("091",  npList.FirstOrDefault(_ => _.Name.Equals("Viettel")).ID),
                new PrefixNP ("094",  npList.FirstOrDefault(_ => _.Name.Equals("Viettel")).ID)
            };
            modelBuilder.Entity<PrefixNP>().HasData(prList);
        }

        private static List<Phone> PhoneGenerateRandom(int sizeOfPhone, int size)
        {
            String s = new String('#', sizeOfPhone);
            Faker<Phone> phoneFaker = new Faker<Phone>()
                    .RuleFor(c => c.Number, f => f.Phone.PhoneNumber(s));
            List<Phone> phones = phoneFaker.Generate(size);
            return phones;
        }
    }
}
