﻿using BeeNumber;
using BeeNumber.Context;
using BeeNumber.Data;
using BeeNumber.UnitOfWork;
using Newtonsoft.Json.Linq;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using TestBeeNumber.DataTest;
using Xunit;

namespace TestBeeNumber
{
    public class FunctionConditionTest
    {
        private IUnitOfWork _unitOfWork;
        private readonly IUnitOfWork _uoW = Substitute.For<IUnitOfWork>();
        private JObject _pc;
        private List<Phone> _listPhone;
        public FunctionConditionTest()
        {
            _listPhone = new List<Phone>();
            for (int i = 4; i <= 10; i++)
            {
                _listPhone.AddRange(Datagenerate.PhoneGenerateRandom(i, 10));
            }
            _pc = Datagenerate.ConfigFileGenerate(true);
            _unitOfWork = new UnitOfWork(new DataContext());
        }

        [Fact]
        public void CheckPhoneGet5to10Char()
        {
            _uoW.GetRepository<Phone>().GetAll().Returns(_listPhone.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            phones = Program.checkNumerAndLong(phones);
            Assert.False(phones.Where(_ => !Program.CheckFormat(_.Number, "[0-9]{"+ _pc ["minlength"] + ","+ _pc["maxlength"] + "}")).Any());
        }

        [Fact]
        public void CheckPhoneGet5to10CharEmptyList()
        {
            List<Phone> emptylist = new List<Phone>();
            _uoW.GetRepository<Phone>().GetAll().Returns(emptylist.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.Empty(phones);
            phones = Program.checkNumerAndLong(phones);
            Assert.Empty(phones);
        }
        [Fact]
        public void CheckPhoneGet5to10CharListPhoneWithoutnumber()
        {
            List<Phone> emptylist = new List<Phone>() {
                new Phone(), new Phone(), new Phone(), new Phone()
            };
            _uoW.GetRepository<Phone>().GetAll().Returns(emptylist.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.NotEmpty(phones);
            phones = Program.checkNumerAndLong(phones);
            Assert.Empty(phones);
        }

        [Fact]
        public void CheckPhone2TabooLast()
        {
            _uoW.GetRepository<Phone>().GetAll().Returns(_listPhone.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            phones = Program.Check2LastNumberTaboo(phones);
            string postFix = "(" + _pc["taboo2LastNumber"].ToList().Aggregate((i, j) => i + "|" + j) + ")$";
            Assert.False(phones.Where(_ => Program.CheckFormat(_.Number, postFix)).Any());
        }

        [Fact]
        public void CheckPhone2TabooLastEmptyList()
        {
            List<Phone> emptylist = new List<Phone>();
            _uoW.GetRepository<Phone>().GetAll().Returns(emptylist.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.Empty(phones);
            phones = Program.Check2LastNumberTaboo(phones);
            Assert.Empty(phones);
        }

        [Fact]
        public void CheckPhone2TabooLastListPhoneWithoutNumber()
        {
            List<Phone> emptylist = new List<Phone>() {
                new Phone(), new Phone(), new Phone(), new Phone()
            };
            _uoW.GetRepository<Phone>().GetAll().Returns(emptylist.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.NotEmpty(phones);
            phones = Program.Check2LastNumberTaboo(phones);
            Assert.Empty(phones);
        }

        [Fact]
        public void CheckPhoneSumDivide55Right()
        {
            _uoW.GetRepository<Phone>().GetAll().Returns(_listPhone.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            phones = phones.Where(_ => Program.CheckConditonFirst5Last5(_.Number));
            foreach (var item in phones)
            {
                Assert.True(Program.CheckConditonFirst5Last5(item.Number));
            }
        }

        [Fact]
        public void CheckPhoneSumDivide55EmptyList()
        {
            List<Phone> emptylist = new List<Phone>();
            _uoW.GetRepository<Phone>().GetAll().Returns(emptylist.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.Empty(phones);
            phones = phones.Where(_ => Program.CheckConditonFirst5Last5(_.Number));
            Assert.Empty(phones);
        }

        [Fact]
        public void CheckPhoneSumDivide55ListPhoneWithoutNumber()
        {
            List<Phone> emptylist = new List<Phone>() {
                new Phone(), new Phone(), new Phone(), new Phone()
            };
            _uoW.GetRepository<Phone>().GetAll().Returns(emptylist.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.NotEmpty(phones);
            phones = phones.Where(_ => Program.CheckConditonFirst5Last5(_.Number));
            Assert.Empty(phones);
        }

        [Fact]
        public void Check2LastNicePairRight()
        {
            _uoW.GetRepository<Phone>().GetAll().Returns(_listPhone.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            phones = Program.Check2LastNicePair(phones);
            string lastNicePairRegex = "(" + _pc["lastNicePair"].ToList().Aggregate((i, j) => i + "|" + j) + ")$";
            Assert.False(phones.Where(_ => !Program.CheckFormat(_.Number, lastNicePairRegex)).Any());
        }

        [Fact]
        public void Check2LastNicePairEmtyList()
        {
            List<Phone> emptylist = new List<Phone>();
            _uoW.GetRepository<Phone>().GetAll().Returns(emptylist.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.Empty(phones);
            phones = Program.Check2LastNicePair(phones);
            Assert.Empty(phones);
        }

        [Fact]
        public void Check2LastNicePairListPhoneWithoutNumber()
        {
            List<Phone> emptylist = new List<Phone>() { 
                new Phone(), new Phone(), new Phone(), new Phone()
            };
            _uoW.GetRepository<Phone>().GetAll().Returns(emptylist.AsQueryable());
            var phones = _uoW.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.NotEmpty(phones);
            phones = Program.Check2LastNicePair(phones);
            Assert.Empty(phones);
        }
    }
}
