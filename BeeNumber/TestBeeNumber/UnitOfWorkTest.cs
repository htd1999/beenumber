using BeeNumber.Context;
using BeeNumber.UnitOfWork;
using BeeNumber;
using Moq;
using System;
using Xunit;
using BeeNumber.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TestBeeNumber
{
    public class UnitOfWorkTest
    {
        private IUnitOfWork _unitOfWork;

        public UnitOfWorkTest()
        {
            _unitOfWork = new UnitOfWork(new DataContext());
        }

        [Fact]
        public void GetPhoneTrue()
        {
            var phone = _unitOfWork.GetRepository<Phone>().Find(_ => _.Number.Equals("0947479634"));
            Assert.NotNull(phone);
            Assert.Equal("0947479634", phone.Number);
        }

        [Fact]
        public void GetAllPhoneTrue()
        {
            var phones = _unitOfWork.GetRepository<Phone>().GetAll().GetAwaiter().GetResult().AsEnumerable();
            Assert.NotNull(phones);
        }

    }
}
