﻿using BeeNumber.Data;
using Bogus;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestBeeNumber.DataTest
{
    public class Datagenerate
    {
        public static List<Phone> PhoneGenerateRandom(int sizeOfPhone,int size)
        {
            String s = new String('#', sizeOfPhone);
            Faker<Phone> phoneFaker = new Faker<Phone>()
                    .RuleFor(c => c.Number, f => f.Phone.PhoneNumber(s));
            List<Phone> phones = phoneFaker.Generate(size);
            return phones;
        }

        public static JObject ConfigFileGenerate(bool rightCase)
        {
            string right = @"{
                              'maxlength': '10',
                              'minlength':  '5',
                              'taboo2LastNumber': [ '00', '66', '04', '45', '85', '27', '67', '17', '57', '97', '98', '58', '42', '82', '69' ],
                              'sumDivide5first5last': [ '24/29', '24/28' ],
                              'lastNicePair': [ '19', '24', '26', '37', '34' ]
                            }";
            string wrong = @"{
                              'maxlength': '10',
                              'minlength':  '5',
                              'taboo2LastNumber': [ '00', '66', '04', '45', '85', '27', '67', '17', '57', '97', '98', '58', '42', '82', '69' ],
                              'sumDivide5first5last': [ '24/29', '24/28' ],
                            }";

            return rightCase ? JObject.Parse(right) : JObject.Parse(wrong);
        }
    }
}
