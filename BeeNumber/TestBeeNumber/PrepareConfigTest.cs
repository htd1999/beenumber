﻿using BeeNumber;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestBeeNumber.DataTest;
using Xunit;

namespace TestBeeNumber
{
    
    public class PrepareConfigTest
    {

        [Fact]
        public void ReadFileRight()
        {
            JObject temp = PrepareConfig.Read();
            Assert.NotNull(temp);
        }

        [Fact]
        public void CheckFileRight()
        {
            JObject temp = PrepareConfig.Read();
            Assert.NotNull(temp);
            Assert.Equal("10", temp["maxlength"]);
            Assert.Equal("5", temp["minlength"]);
            Assert.Equal(15, temp["taboo2LastNumber"].ToList().Count());
            Assert.Equal(5, temp["lastNicePair"].ToList().Count());
        }

        [Fact]
        public void CheckFileEmptyParameter()
        {
            JObject temp = Datagenerate.ConfigFileGenerate(false);
            Assert.NotNull(temp);
            Assert.Null(temp["lastNicePair"]);
        }

        [Fact]
        public void CheckFunctionCheck()
        {
            JObject temp = Datagenerate.ConfigFileGenerate(false);
            Assert.Throws<ArgumentNullException>(() => PrepareConfig.Check(temp));
        }
    }
}
